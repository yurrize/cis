package com.tec_do.cis_last;

import com.tec_do.cis_last.dto.DataDTO;
import com.tec_do.cis_last.service.CRMService;
import com.tec_do.cis_last.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class CisLastApplicationTests {

    @Resource
    private CRMService crmService;

    @Resource
    private UserService userService;

    @Test
    void contextLoads() {

//        userService.getUserMailNyId("ou_29fea11ad65b0f4a58a0eb8c6dc61b66");


        DataDTO dataDTO = crmService.loginCRM("join.su@tec-do.com");

        crmService.updateBase(dataDTO,"ou_dfde299680ec89f0ae71131631b1989c");

        System.out.println(dataDTO);


    }

}

package com.tec_do.cis_last.dto;

import lombok.Data;

@Data
public class AdminDTO {
    private String username;
    private String password;
}

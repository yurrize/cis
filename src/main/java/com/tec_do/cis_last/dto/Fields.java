package com.tec_do.cis_last.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Fields {
    private String companyName;
    private String customerName;
    private String phone;
    private String address;
    private ArrayList<User> userArrayList;
}

package com.tec_do.cis_last.dto;

import lombok.Data;

@Data
public class ProgramKeyDTO {
    private String app_id;
    private String app_secret;
}

package com.tec_do.cis_last.dto;


import lombok.Data;

import java.util.List;

@Data
public class RecordsDTO {

    private List<Fields> fieldsList;
}

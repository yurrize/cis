package com.tec_do.cis_last.dto;


import lombok.Data;

@Data
public class Contact {

    private String uec;
    private String address;
    private String phone;
    private String name;
    private int id;
}

package com.tec_do.cis_last.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataDTO {
    private String name;
    private String dept;
    private List<Customer> customers;

}

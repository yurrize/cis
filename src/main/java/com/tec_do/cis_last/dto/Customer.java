package com.tec_do.cis_last.dto;

import lombok.Data;

import java.util.List;

@Data
public class Customer {
    private String name;
    private int id;
    private List<Contact> contacts;
}

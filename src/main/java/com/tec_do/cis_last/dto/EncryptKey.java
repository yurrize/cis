package com.tec_do.cis_last.dto;

import lombok.Data;

@Data
public class EncryptKey {
    private String challenge;
    private String token;
    private String type;
}

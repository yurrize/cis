package com.tec_do.cis_last.dto;

import lombok.Data;

@Data
public class ThirdPartyTokenDTO {
    private String app_access_token;
    private Integer code;
    private Long expire;
    private String msg;
    private String tenant_access_token;

}

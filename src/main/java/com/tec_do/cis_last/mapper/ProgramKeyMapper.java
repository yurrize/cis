package com.tec_do.cis_last.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tec_do.cis_last.pojo.ProgramKey;
import org.apache.ibatis.annotations.Param;

public interface ProgramKeyMapper extends BaseMapper<ProgramKey> {

    ProgramKey queryProgramKeyByType(@Param("type")Integer typeId);

}

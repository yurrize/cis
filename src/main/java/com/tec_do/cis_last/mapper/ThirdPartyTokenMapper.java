package com.tec_do.cis_last.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tec_do.cis_last.pojo.ThirdPartyToken;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ThirdPartyTokenMapper extends BaseMapper<ThirdPartyToken> {

    Boolean updateThirdPartyToken(@Param("token") ThirdPartyToken token);
}

package com.tec_do.cis_last.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tec_do.cis_last.pojo.BDCustomer;

public interface BDCustomerMapper extends BaseMapper<BDCustomer> {
}

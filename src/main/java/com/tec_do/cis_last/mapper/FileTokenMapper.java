package com.tec_do.cis_last.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tec_do.cis_last.pojo.FileToken;

public interface FileTokenMapper extends BaseMapper<FileToken> {
    FileToken queryFileTokenByType(Integer environmentId);
}

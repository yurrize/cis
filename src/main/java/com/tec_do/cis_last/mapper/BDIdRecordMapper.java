package com.tec_do.cis_last.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tec_do.cis_last.pojo.BDIdRecord;

public interface BDIdRecordMapper extends BaseMapper<BDIdRecord> {
}

package com.tec_do.cis_last.task;

import com.tec_do.cis_last.dto.ThirdPartyTokenDTO;
import com.tec_do.cis_last.service.ThirdPartyTokenService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

//定时刷新API访问凭证

@Component
public class AccessTokenRefreshTask {
    /**
     * 定时刷新AccessToken任务
     * 0是测试环境，1是生产环境，更换就好，以后可以配置到yaml
     * 根据type环境，查询programkey表获取app两件套
     * 返回来，拿这个两个返回值访问飞书API，换取访问凭证AccessToken，刷新入数据库
     */

    @Resource
    private ThirdPartyTokenService thirdPartyTokenService;

    @Scheduled(fixedRate = 4120000) // 每1.2小时执行一次
    public void refreshAccessToken() {
        try {
            // 调用飞书云API刷新访问凭证
            ThirdPartyTokenDTO newAccessToken = thirdPartyTokenService.refreshAccessTokenFromLark();

            // 更新缓存或数据库中的凭证数据
            Boolean flag = thirdPartyTokenService.updateAccessTokenInCache(newAccessToken);

            if (flag) {
                System.out.println("Access token refreshed successfully.");
            }else {
                System.out.println("Access token refreshed fail.");
            }

        } catch (Exception e) {
            System.err.println("Error refreshing access token: " + e.getMessage());
        }
    }

}

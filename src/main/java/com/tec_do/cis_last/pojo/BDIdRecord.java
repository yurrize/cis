package com.tec_do.cis_last.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("bd_id_record")
public class BDIdRecord {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String userId;

    private String email;

    private Integer status;
}

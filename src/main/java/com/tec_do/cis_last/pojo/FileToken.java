package com.tec_do.cis_last.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("file_token")
public class FileToken {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String appToken;

    private String tableId;

    private Integer type;

}

package com.tec_do.cis_last.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("third_party_access_tokens")
public class ThirdPartyToken {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String appName;
    private String accessToken;
    private String tenantAccessToken;

    //凭证过期时间
    private LocalDateTime expirationTime;

    //创建时间
    private LocalDateTime createdAt;
}

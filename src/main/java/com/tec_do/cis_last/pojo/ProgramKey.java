package com.tec_do.cis_last.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

//应用关键信息类
@Data
@TableName("programkey")
public class ProgramKey {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String appId;
    private String appSecret;

    private String type;
}

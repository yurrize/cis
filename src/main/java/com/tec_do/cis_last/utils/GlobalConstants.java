package com.tec_do.cis_last.utils;

import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConstants {
    //飞书API通用请求路径
    public static final String feishuURL = "https://open.feishu.cn/open-apis";

    //CRM接口地址
//    public static final String crmURL = "http://172.24.2.118:50023";


    public static final String crmURL = "https://bpms.tec-do.com";


    //AdminUser,测试环境
//    public static final String AdminUserName = "billing-api-service";

    //生产环境
    public static final String AdminUserName = "opt-api-service";

    //生产环境
}

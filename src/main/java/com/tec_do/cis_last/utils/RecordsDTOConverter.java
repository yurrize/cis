package com.tec_do.cis_last.utils;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.tec_do.cis_last.dto.Fields;
import com.tec_do.cis_last.dto.RecordsDTO;

public class RecordsDTOConverter {
    public static JSONObject convertToJSON(RecordsDTO recordsDTO){
        JSONObject result = new JSONObject();
        JSONArray recordsArray = new JSONArray();

        for (Fields fields : recordsDTO.getFieldsList()) {
            JSONObject fieldsJSON = new JSONObject();
            JSONObject fieldsInnerJSON = new JSONObject();

            fieldsInnerJSON.put("客户名称", fields.getCompanyName());
            fieldsInnerJSON.put("联系人姓名", fields.getCustomerName());
            fieldsInnerJSON.put("客户所有人", fields.getUserArrayList());
            fieldsInnerJSON.put("联系电话", fields.getPhone());
            fieldsInnerJSON.put("客户地址",fields.getAddress());

            fieldsJSON.put("fields", fieldsInnerJSON);
            recordsArray.add(fieldsJSON);
        }

        result.put("records", recordsArray);

        System.out.println(result);
        return result;
    }
}

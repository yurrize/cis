package com.tec_do.cis_last;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.tec_do.cis_last.mapper")
@EnableScheduling
public class CisLastApplication {

    public static void main(String[] args) {
        SpringApplication.run(CisLastApplication.class, args);
    }

}

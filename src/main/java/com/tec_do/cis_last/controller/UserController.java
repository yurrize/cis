package com.tec_do.cis_last.controller;

import cn.hutool.http.server.HttpServerRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.tec_do.cis_last.dto.Result;
import com.tec_do.cis_last.service.BDIdRecordService;
import com.tec_do.cis_last.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private BDIdRecordService bdIdRecordService;

    @PostMapping("/login")
    private Result userLogin(@RequestBody JSONObject jsonObject, HttpServerRequest request){
//        System.out.println(jsonObject.toString());



        // 解析 JSON 请求体
        JSONObject event = jsonObject.getJSONObject("event");


        if (!event.containsKey("operator_id_list")) {
            System.out.println("空参报错");
            return Result.fail("null error");
        }

        JSONArray operatorIdList = event.getJSONArray("operator_id_list");

        // 获取 operator_id_list 中的第一个元素的 user_id
        String userId = operatorIdList.getJSONObject(0).getStr("open_id");

        if (userId.isEmpty()) {
            throw new RuntimeException(request.getURI()+"user_id获取失败,"+"requestBody为"+ jsonObject);
        }

//        System.out.println(request.getHeaders());

        // 现在您可以使用 userId 进行进一步的处理
        System.out.println("userId为: " + userId);

        if (bdIdRecordService.saveUserId(userId)) {
            System.out.println("用户已存在");
        }
        String msg = userService.getUserMailNyId(userId);
        System.out.println(msg);

        return Result.ok(msg);
    }


//    @PostMapping("/login")
    //这个是拿来给飞书验证接口用的
    private JSONObject userLogin(@RequestBody JSONObject jsonObject){

        System.out.println(jsonObject);

        String challenge = jsonObject.getStr("challenge");

        System.out.println(challenge);

        JSONObject entries = new JSONObject();
        entries.set("challenge",challenge);

        return entries;
    }
}

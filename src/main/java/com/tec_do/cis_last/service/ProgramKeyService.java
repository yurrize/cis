package com.tec_do.cis_last.service;

import com.tec_do.cis_last.pojo.ProgramKey;

public interface ProgramKeyService {
    ProgramKey getKeyByType(Integer environmentId);
}

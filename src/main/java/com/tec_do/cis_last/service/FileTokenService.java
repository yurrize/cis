package com.tec_do.cis_last.service;

import com.tec_do.cis_last.pojo.FileToken;

public interface FileTokenService {
    FileToken getKeyByType();
}

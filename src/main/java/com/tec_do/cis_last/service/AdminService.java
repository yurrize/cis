package com.tec_do.cis_last.service;

import com.tec_do.cis_last.dto.AdminDTO;

public interface AdminService {
    AdminDTO getAdminInfo();
}

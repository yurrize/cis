package com.tec_do.cis_last.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.dto.AdminDTO;
import com.tec_do.cis_last.mapper.AdminMapper;
import com.tec_do.cis_last.pojo.Admin;
import com.tec_do.cis_last.service.AdminService;
import com.tec_do.cis_last.utils.GlobalConstants;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    //获取admin
    @Override
    public AdminDTO getAdminInfo() {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name", GlobalConstants.AdminUserName);

        Admin admin = getOne(queryWrapper);
        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setUsername(admin.getUserName());
        adminDTO.setPassword(admin.getPassword());

        return adminDTO;

    }
}

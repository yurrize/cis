package com.tec_do.cis_last.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tec_do.cis_last.dto.DataDTO;
import com.tec_do.cis_last.service.BDIdRecordService;
import com.tec_do.cis_last.service.CRMService;
import com.tec_do.cis_last.service.ThirdPartyTokenService;
import com.tec_do.cis_last.service.UserService;
import com.tec_do.cis_last.utils.GlobalConstants;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private ThirdPartyTokenService thirdPartyTokenService;

    @Resource
    private CRMService crmService;

    @Resource
    private BDIdRecordService bdIdRecordService;


    @Override
    public String getUserMailNyId(String userId) {


        String tenantAccessToken = thirdPartyTokenService.queryTokenByName("司庆送礼小程序");

        String realUrl = GlobalConstants.feishuURL + "/contact/v3/users/"+userId+"?department_id_type=open_department_id&user_id_type=open_id";

        // 设置 HTTP 请求头
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "Bearer " + tenantAccessToken);

        // 创建带有请求头的 HttpEntity
        HttpEntity<String> requestEntity = new HttpEntity<>(httpHeaders);

        // 创建 RestTemplate 实例
        RestTemplate restTemplate = new RestTemplate();

        // 发送 GET 请求并接收响应
        ResponseEntity<String> response = restTemplate.exchange(
                realUrl,
                HttpMethod.GET,
                requestEntity,
                String.class
        );

        // 检查响应的状态码是否成功（例如，200 OK）
        if (response.getStatusCode() == HttpStatus.OK) {
            String responseBody = response.getBody();

            // 解析 JSON 响应并从 data 部分提取 email 字段
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode jsonNode = objectMapper.readTree(responseBody);
                String email = jsonNode.path("data").path("user").path("email").asText();

                if (email.isEmpty()) {
                    throw new RuntimeException("email is null");
                }

                Boolean flag = bdIdRecordService.updateEmailById(userId,email);

                if (!flag){
                    return "user update fail";
                }

                DataDTO dataDTO = crmService.loginCRM(email);

                if (BeanUtil.isEmpty(dataDTO)) {
                    return null;
                }

                // 检查是否需要更新状态
                if (bdIdRecordService.needUpdateStatus(userId)) {
                    // 尝试从CRM服务更新基本数据
                    if (crmService.updateBase(dataDTO, userId)) {
                        // 如果更新成功，更新状态
                        bdIdRecordService.updateStatus(userId);
                        return "更新成功";
                    }
                }

                return "已更新或更新失败";

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }


        return "已更新或更新失败";
    }



}

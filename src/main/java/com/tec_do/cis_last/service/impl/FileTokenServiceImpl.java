package com.tec_do.cis_last.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.mapper.FileTokenMapper;
import com.tec_do.cis_last.pojo.FileToken;
import com.tec_do.cis_last.service.FileTokenService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class FileTokenServiceImpl extends ServiceImpl<FileTokenMapper, FileToken> implements FileTokenService {
    @Value("${environment-type}")
    private Integer environmentId;


    @Resource
    private FileTokenMapper fileTokenMapper;

    @Override
    public FileToken getKeyByType() {
        return fileTokenMapper.queryFileTokenByType(environmentId);
    }
}

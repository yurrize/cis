package com.tec_do.cis_last.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.mapper.BDMapper;
import com.tec_do.cis_last.pojo.BD;
import com.tec_do.cis_last.service.BDService;
import org.springframework.stereotype.Service;

@Service
public class BDServiceImpl extends ServiceImpl<BDMapper, BD> implements BDService {
}

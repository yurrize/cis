package com.tec_do.cis_last.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.mapper.ProgramKeyMapper;
import com.tec_do.cis_last.pojo.ProgramKey;
import com.tec_do.cis_last.service.ProgramKeyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ProgramKeyServiceImpl extends ServiceImpl<ProgramKeyMapper, ProgramKey> implements ProgramKeyService {

    @Resource
    private ProgramKeyMapper programKeyMapper;

    @Override
    public ProgramKey getKeyByType(Integer environmentId) {
        return programKeyMapper.queryProgramKeyByType(environmentId);
    }
}

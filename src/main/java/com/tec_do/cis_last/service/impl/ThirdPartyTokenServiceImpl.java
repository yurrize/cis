package com.tec_do.cis_last.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.dto.ProgramKeyDTO;
import com.tec_do.cis_last.dto.ThirdPartyTokenDTO;
import com.tec_do.cis_last.mapper.ThirdPartyTokenMapper;
import com.tec_do.cis_last.pojo.ProgramKey;
import com.tec_do.cis_last.pojo.ThirdPartyToken;
import com.tec_do.cis_last.service.ProgramKeyService;
import com.tec_do.cis_last.service.ThirdPartyTokenService;
import com.tec_do.cis_last.utils.GlobalConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class ThirdPartyTokenServiceImpl extends ServiceImpl<ThirdPartyTokenMapper, ThirdPartyToken> implements ThirdPartyTokenService {

    @Resource
    private ProgramKeyService programKeyService;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");

    @Resource
    private ThirdPartyTokenMapper thirdPartyTokenMapper;

    @Value("${environment-type}")
    private Integer environmentId;

    @Override
    // 调用飞书云API刷新访问凭证
    public ThirdPartyTokenDTO refreshAccessTokenFromLark() {
        RestTemplate restTemplate = new RestTemplate();

        String appAccessTokenURL = GlobalConstants.feishuURL+"/auth/v3/tenant_access_token/internal";

        ProgramKey programKey = programKeyService.getKeyByType(environmentId);

        ProgramKeyDTO programKeyDTO = new ProgramKeyDTO();
        programKeyDTO.setApp_id(programKey.getAppId());
        programKeyDTO.setApp_secret(programKey.getAppSecret());

        //设置请求头
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.set("Content-Type","application/json; charset=utf-8");

        HttpEntity<String> fromEntity = new HttpEntity<>(JSONUtil.toJsonStr(programKeyDTO), httpHeaders);


        System.out.println(fromEntity);

        String response = restTemplate.postForObject(appAccessTokenURL, fromEntity, String.class);

        return JSONUtil.toBean(response, ThirdPartyTokenDTO.class);

    }


    @Override
    //更新数据库表
    public Boolean updateAccessTokenInCache(ThirdPartyTokenDTO newAccessToken) {
        ThirdPartyToken token = new ThirdPartyToken();
        token.setAccessToken(newAccessToken.getApp_access_token());
        token.setTenantAccessToken(newAccessToken.getTenant_access_token());
        token.setCreatedAt(LocalDateTime.now());

        Duration duration = Duration.ofSeconds(newAccessToken.getExpire());
        token.setExpirationTime(LocalDateTime.now().plus(duration));

        token.setAppName("司庆送礼小程序");

        return thirdPartyTokenMapper.updateThirdPartyToken(token);
    }


    //根据应用名字获取access_token
    public String queryTokenByName(String name){
        QueryWrapper<ThirdPartyToken> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("app_name","司庆送礼小程序");
        ThirdPartyToken token = getOne(queryWrapper);
        return token.getTenantAccessToken();
    }

}

package com.tec_do.cis_last.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.mapper.BDCustomerMapper;
import com.tec_do.cis_last.pojo.BDCustomer;
import com.tec_do.cis_last.service.BDCustomerService;
import org.springframework.stereotype.Service;


@Service
public class BDCustomerServiceImpl extends ServiceImpl<BDCustomerMapper, BDCustomer> implements BDCustomerService {
}

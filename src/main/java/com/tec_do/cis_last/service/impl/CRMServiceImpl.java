package com.tec_do.cis_last.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.tec_do.cis_last.dto.*;
import com.tec_do.cis_last.pojo.FileToken;
import com.tec_do.cis_last.service.AdminService;
import com.tec_do.cis_last.service.CRMService;
import com.tec_do.cis_last.service.FileTokenService;
import com.tec_do.cis_last.service.ThirdPartyTokenService;
import com.tec_do.cis_last.utils.GlobalConstants;
import com.tec_do.cis_last.utils.RecordsDTOConverter;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CRMServiceImpl implements CRMService {


    @Resource
    private AdminService adminService;

    @Resource
    private ThirdPartyTokenService thirdPartyTokenService;


    @Resource
    private FileTokenService fileTokenService;

    @Override
    //登录换token
    public DataDTO loginCRM(String email) {

        AdminDTO adminInfo = adminService.getAdminInfo();

        RestTemplate restTemplate = new RestTemplate();


        //crm接口放这里
        String crmRequestURL = GlobalConstants.crmURL+"/bpms/login.jsp";

        //设置请求头
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.set("Content-Type","application/json");


        HttpEntity<String> fromEntity = new HttpEntity<>(JSONUtil.toJsonStr(adminInfo), httpHeaders);

        ResponseEntity<String> response = restTemplate.postForEntity(crmRequestURL, fromEntity, String.class);


         //获取响应头中的 X-CSRF-Token
        List<String> csrfTokens = response.getHeaders().get("X-CSRF-Token");
        List<String> list = response.getHeaders().get("Set-Cookie");
        if (csrfTokens == null || csrfTokens.isEmpty()||list==null||list.isEmpty()) {
            return null;
        }

        String cookie = csrfTokens.get(0)+ ";" +list.get(0);
        System.out.println(cookie);
        return getCRM(email, cookie);

    }


    //CRM拉取数据
    private DataDTO getCRM(String email,String cookie){

        RestTemplate restTemplate = new RestTemplate();
        String crmRequestURL =  GlobalConstants.crmURL+"/bpms/ws/api/opt/gift/customerInfo?email="+email;

        HttpHeaders httpHeaders = new HttpHeaders();


        httpHeaders.set("Cookie",cookie);


        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);

        // 发送 GET 请求，将HttpEntity对象作为参数传递
        ResponseEntity<String> response = restTemplate.exchange(crmRequestURL, HttpMethod.GET, httpEntity, String.class);

        if (!(response.getStatusCode()== HttpStatus.OK)){
            System.out.println("error");
        }

        String responseBody = response.getBody();

        JSONObject jsonObject = new JSONObject(responseBody);

        System.out.println(jsonObject);

        Integer status = jsonObject.getInt("status");

        if (!jsonObject.containsKey("data")||status!=0) {
            System.out.println("没有客户信息");
            return null;
        }

        JSONObject dataObject = jsonObject.getJSONObject("data");


        String dataAsString = dataObject.toString();

        DataDTO bean = JSONUtil.toBean(dataAsString, DataDTO.class);


//        System.out.println("DataDTO的值为:"+bean);


        if (BeanUtil.isEmpty(bean)) {
            throw new RuntimeException("crm data is null");
        }

        return bean;
    }

    //更新多维表格
    public Boolean updateBase(DataDTO bean,String userId){

        String tenantAccessToken = thirdPartyTokenService.queryTokenByName("司庆送礼小程序");


        FileToken fileToken = fileTokenService.getKeyByType();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+tenantAccessToken);

        //处理请求体
        List<Customer> customerList = bean.getCustomers();

        ArrayList<Fields> fields = new ArrayList<>();

        User user = new User();
        user.setId(userId);
        ArrayList<User> users = new ArrayList<>();
        users.add(user);

        for (Customer customer : customerList) {
            List<Contact> contacts = customer.getContacts();

            // 判断客户名单是否为空
            if (contacts.isEmpty()) {
                Fields field = new Fields();
                field.setCompanyName(customer.getName());
                field.setUserArrayList(users);
                fields.add(field);
            } else {
                for (Contact contact : contacts) {
                    Fields field = new Fields(); // 创建新的Fields对象
                    field.setCompanyName(customer.getName());
                    field.setCustomerName(contact.getName());
                    field.setAddress(contact.getAddress());
                    field.setPhone(contact.getPhone());
                    field.setUserArrayList(users);
                    fields.add(field);
                }
            }
        }


        RecordsDTO records = new RecordsDTO();
        records.setFieldsList(fields);

        System.out.println(records);

        JSONObject entries = RecordsDTOConverter.convertToJSON(records);

        if (entries.isEmpty()) {
            System.out.println("entries请求体为空");
            return false;
        }

        String entriesString = entries.toString();


        HttpEntity<String> fromEntity = new HttpEntity<>(JSONUtil.toJsonStr(entriesString), headers);

        String updateURL = GlobalConstants.feishuURL+"/bitable/v1/apps/"+fileToken.getAppToken()+"/tables/"+fileToken.getTableId()+"/records/batch_create?user_id_type=open_id";



        System.out.println(updateURL);


        String postForObject = restTemplate.postForObject(updateURL, fromEntity, String.class);
        System.out.println(postForObject);

        JSONObject jsonObject = new JSONObject(postForObject);

        Integer code = jsonObject.getInt("code");

        return code == 0;

    }



}

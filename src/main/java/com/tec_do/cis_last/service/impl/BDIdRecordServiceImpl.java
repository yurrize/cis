package com.tec_do.cis_last.service.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.mapper.BDIdRecordMapper;
import com.tec_do.cis_last.pojo.BDIdRecord;
import com.tec_do.cis_last.service.BDIdRecordService;
import org.springframework.stereotype.Service;

@Service
public class BDIdRecordServiceImpl extends ServiceImpl<BDIdRecordMapper, BDIdRecord>implements BDIdRecordService {
    @Override
    public Boolean saveUserId(String userId) {
        BDIdRecord bdIdRecord = new BDIdRecord();
        bdIdRecord.setUserId(userId);

        QueryWrapper<BDIdRecord> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("user_id",userId);

        BDIdRecord one = getOne(queryWrapper);

        if (BeanUtil.isEmpty(one)){
            save(bdIdRecord);
            return true;
        }
        return false;

    }

    @Override
    public Boolean updateEmailById(String userId, String email) {

        UpdateWrapper<BDIdRecord> updateWrapper = new UpdateWrapper<>();

        updateWrapper.eq("user_id",userId);

        updateWrapper.set("email",email);

        updateWrapper.last("limit 1");

        return update(updateWrapper);


    }


    //判断更新状态
    @Override
    public boolean needUpdateStatus(String userId) {

        QueryWrapper<BDIdRecord> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("user_id",userId);

        BDIdRecord one = getOne(queryWrapper);

        Integer status = one.getStatus();
        return status == 0;
    }

    @Override
    public Boolean updateStatus(String userId) {

        UpdateWrapper<BDIdRecord> updateWrapper = new UpdateWrapper<>();

        updateWrapper.eq("user_id",userId);

        updateWrapper.set("status",1);
        return update(updateWrapper);
    }


}

package com.tec_do.cis_last.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tec_do.cis_last.mapper.CompanyMapper;
import com.tec_do.cis_last.pojo.Company;
import com.tec_do.cis_last.service.CompanyService;
import org.springframework.stereotype.Service;


@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company>implements CompanyService {
}

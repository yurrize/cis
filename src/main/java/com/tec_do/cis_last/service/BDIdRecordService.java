package com.tec_do.cis_last.service;

public interface BDIdRecordService {
    Boolean saveUserId(String userId);

    Boolean updateEmailById(String userId, String email);

    boolean needUpdateStatus(String userId);

    Boolean updateStatus(String userId);
}

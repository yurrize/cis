package com.tec_do.cis_last.service;

import com.tec_do.cis_last.dto.ThirdPartyTokenDTO;

public interface ThirdPartyTokenService {
    ThirdPartyTokenDTO refreshAccessTokenFromLark();

    Boolean updateAccessTokenInCache(ThirdPartyTokenDTO newAccessToken);

    String queryTokenByName(String name);
}

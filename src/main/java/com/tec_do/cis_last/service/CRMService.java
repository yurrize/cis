package com.tec_do.cis_last.service;

import com.tec_do.cis_last.dto.DataDTO;

public interface CRMService {
    DataDTO loginCRM(String email);

    Boolean updateBase(DataDTO bean,String userId);

}
